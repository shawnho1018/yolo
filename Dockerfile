FROM python:3.8-slim-buster
RUN apt-get update
RUN apt install -y ffmpeg libsm6 libxext6
ADD . /app
WORKDIR /app
RUN python -m pip install --upgrade pip 
RUN pip install -r requirements.txt
EXPOSE 8080
CMD ["python", "webapp.py", "--port=8080"]