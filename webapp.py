"""
Simple app to upload an image via a web form 
and view the inference results on the image in the browser.
"""
import argparse
import io
import os
from PIL import Image
from yolov5 import YOLOv5
from flask import Flask, render_template, request, redirect

app = Flask(__name__)
DETECTION_URL = "/v1/predict"
@app.route("/", methods=["GET", "POST"])
def predict():
    if request.method == "POST":
        if "file" not in request.files:
            return redirect(request.url)
        file = request.files["file"]
        if not file:
            print("Not a file format")
            return

        img_bytes = file.read()
        print("Before open image file1")
        img = Image.open(io.BytesIO(img_bytes))
        print("before input into model")
        results = yolov5.predict(img, size=640, augment=False)

        results.render()  # updates results.imgs with boxes and labels
        for img in results.imgs:
            img_base64 = Image.fromarray(img)
            img_base64.save("static/image0.jpg", format="JPEG")
        return redirect("static/image0.jpg")

    return render_template("index.html")

@app.route(DETECTION_URL, methods=["POST"])
def inference():
    if not request.method == "POST":
        return

    if request.files.get("image"):
        image_file = request.files["image"]
        image_bytes = image_file.read()

        img = Image.open(io.BytesIO(image_bytes))

        results = model(img, size=640)
        data = results.pandas().xyxy[0].to_json(orient="records")
        return data

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Flask app exposing yolov5 models")
    parser.add_argument("--port", default=8080, type=int, help="port number")
    args = parser.parse_args()

    device = "cpu"
    yolov5 = YOLOv5("yolov5s.pt", device, load_on_init=True)
    app.run(host="0.0.0.0", port=args.port)  # debug=True causes Restarting with stat
